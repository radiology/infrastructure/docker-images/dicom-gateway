# DICOM-gateway

Health-Data-Platform DICOM gateway

# Setup dev environment

Create local kubernetes environment using (including nginx ingress):
```bash
k3d cluster create --config .develop/config.yaml

# Install NGINX-ingress:
helm install --namespace ingress-nginx --create-namespace ingress-nginx https://github.com/kubernetes/ingress-nginx/releases/download/helm-chart-4.0.17/ingress-nginx-4.0.17.tgz
```