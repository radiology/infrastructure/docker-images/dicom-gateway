#!/usr/bin/env python
import logging
import socket
import os
import shutil
from pathlib import Path

import click
from click_loglevel import LogLevel
import yaml
import studyclient

from pydicom.fileset import FileSet
from pydicom import dcmread
from pydicom.errors import InvalidDicomError
from subprocess import run


class DicomGatewayProcessor():
    def __init__(self, path):
        self.path = path
        self.fs = None
        self.patient_id = None
        self.accession_number = None
        self._study_client = None
        self._experiment = None
        self._callback_execution = None
        self.file_counts = None
        self.ctp_result = None

    def process(self):
        self.process_path()
        self.check_fileset()
        self.send_meta_data()
        self.get_destination()
        self.send_to_CTP()
        self.update_studygovernor()

    @property
    def study_client(self):
        if self._study_client is None:
            self._study_client = studyclient.connect(
                uri="http://hdp-demo-studygovernor.hdp-governor.svc.cluster.local:8000/",
                user='admin',
                password='blaat'
            )

        return self._study_client

    @property
    def callback_execution(self):
        if self._callback_execution is None:
            if self.accession_number is None:
                return None

            # Get experiment by accession_number
            if self._experiment is None:
                self._experiment = self.study_client.get_experiment(self.accession_number)

            if self._experiment is None:
                return None

            # Check last action
            action = self._experiment.actions[-1]

            # Make sure action only has on callback execution
            executions = action.executions
            if len(executions) != 1:
                return None

            execution = executions[0]

            # Make sure the execution is waiting
            if execution.status != 'waiting':
                return None

            self._callback_execution = execution

        return self._callback_execution

    def _validate_dicom_tags(self, ds):
        # Validate StudyDate, StudyTime, StudyID
        required_tags = {
            'StudyID': 'DUMMY',
            'StudyDate': '01-01-1970',
            'StudyTime': '12:00:00'
        }
        for tag, default_value in required_tags.items():
            try:
                if ds.__getattr__(tag) == '':
                    ds.__setattr__(tag, default_value)
            except AttributeError:
                ds.__setattr__(tag, default_value)

    # required DICOM tags:
    # ["StudyDate", "StudyTime", "StudyID"]
    def process_path(self):
        logging.info(f"{self.path.name} - Processing path.")
        self.fs = FileSet()
        files = [file for file in self.path.glob("**/*") if file.is_file()]
        for file in files:
            logging.debug(f"{self.path.name} - Reading file '{file}'")
            try:
                ds = dcmread(file, stop_before_pixels=True)
                # CHeck if studyID is set (Required DicomTAG)
                self._validate_dicom_tags(ds)
                self.fs.add(ds)
            except InvalidDicomError:
                logging.warning(f"{self.path.name} - Cannot add file '{file}' not a dicom")

    def check_fileset(self):
        logging.info(f"{self.path.name} - Processing FileSet")
        patient_ids = self.fs.find_values("PatientID")
        accession_numbers = self.fs.find_values("AccessionNumber")
        if len(patient_ids) != 1:
            logging.error(f"{self.path.name} - Data contains multiple patient ID's")
        if len(accession_numbers) != 1:
            logging.error(f"{self.path.name} - Data contains multiple patient ID's")
        self.patient_id = patient_ids[0]
        self.accession_number = accession_numbers[0]
        logging.info(f'{self.path.name} - Subject: {self.patient_id} AccessionNumber: {self.accession_number}')

    def send_meta_data(self):
        logging.info(f"{self.path.name} - Get Meta data")
        series_instance_uids = self.fs.find_values("SeriesInstanceUID")
        self.file_counts = {}
        for series_instance_uid in series_instance_uids:
            files = self.fs.find(SeriesInstanceUID=series_instance_uid)
            logging.info(f'{self.path.name} - Serie: {series_instance_uid} Files: {len(files)}')
            self.file_counts[series_instance_uid] = len(files)

        # Send result to StudyGov
        self.callback_execution.set_fields(
            run_log="File arrived in Dicom Gateway and metadata extracted.",
            result_values={
                "patient_id": self.patient_id,
                "accession_number": self.accession_number,
                "file_counts": self.file_counts,
            }
        )

    def get_destination(self):
        # Mocking for now
        study_governor = StudyGovernorMock(Path('/data/sg-mock-config/data.yaml'))
        result = study_governor.get(self.patient_id, self.accession_number)
        logging.info(result)
        if not result:
            logging.error(f'{self.path.name} - Unable to get destination')
            self.destination_host = None
            self.destination_port = None
            return
        self.destination_host = result['external_host']
        self.destination_port = str(result['port'])

    def send_to_CTP(self):
        if not self.destination_host:
            logging.error(f'{self.path.name} - Unable to send dataset')
            return
        logging.info(f"{self.path.name} - Send to {self.destination_host}:{self.destination_port}")
        p = run([ 'dcmsend', '--scan-directories', self.destination_host, self.destination_port, self.path], capture_output=True)

        self.ctp_result = {
            "return_code": p.returncode,
            "stdout": p.stdout.decode(),
            "stderr": p.stderr.decode(),
        }

        logging.info(f'exit status: {p.returncode}')
        logging.info(f'stdout: {p.stdout.decode()}')
        logging.info(f'stderr: {p.stderr.decode()}')

    def update_studygovernor(self):
        self.callback_execution.resolve(
            status='finished',
            result='success',
            result_values={
                "patient_id": self.patient_id,
                "accession_number": self.accession_number,
                "file_counts": self.file_counts,
                "destination_host": self.destination_host,
                "destination_port": self.destination_port,
                "ctp_result": self.ctp_result,
            }
        )


class StudyGovernorMock:

    def __init__(self, path):
        self._data = {}
        if not path.is_file():
            logging.critical(f'Cannot find file {path}')
            return
        try:
            with open(path, 'r') as file:
                self._data = yaml.load(file, Loader=yaml.FullLoader)
        except IOError:
            logging.critical(f'Unable to open studygovernor MOCK data')
        except yaml.YAMLError as e:
            logging.critical(f'Unable to parse studygovernor MOCK YAML')
            logging.critical(f'  {str(e.problem_mark)}')
            logging.critical(f'  {str(e.problem)} {str(e.context)}')

    def get(self, patient_id, accession_nr):
        result = None
        try:
            result = self._data[patient_id]['studies'][accession_nr]
        except KeyError:
            return None
        return result


@click.command()
@click.argument('directory')
@click.option('-l', '--log-level', type=LogLevel(), default=logging.INFO)
def study_received(directory, log_level):
    # Setup logging
    logging.basicConfig(
        format=f"%(asctime)s - {socket.gethostname()} - [%(levelname)-8s] %(message)s",
        level=log_level,
    )

    # Check input
    path = Path(directory)
    if not path.is_dir():
        logging.critical(f"{path.name} is not a directory")
        return
    if not os.access(path, os.R_OK):
        logging.critical(f"{path.name} is not readable")
        return

    dicom_gateway_processor = DicomGatewayProcessor(path)
    dicom_gateway_processor.process()
    shutil.rmtree(path)


if __name__ == "__main__":
    study_received()
